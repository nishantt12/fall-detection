package com.falldetections;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by nishant on 16/4/16.
 */
public class IncomingSms extends BroadcastReceiver {

    Context context;

    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();

        this.context = context;

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();


                    if (message.toLowerCase().equals("fall")) {
//                        Toast.makeText(context, "senderNum: " + senderNum + ", message: " + message, Toast.LENGTH_LONG).show();
                    }


                    if (senderNum.contains("8802087693")) {

                        sendNotificationShortText("Fall Detection", "Fall Detected");
                    }
//                    sendNotificationShortText("Fall Detection", "Fall Detected");


                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
            // //////////Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }


        sendNotificationShortText("Fall Detection", "Fall Detected");

    }


    private Intent getIntentToStart() {
        Class activityToStart = null;
        Intent intent = null;

        activityToStart = MainActivity.class;
        intent = new Intent(context, activityToStart);

        return intent;

    }


    private void sendNotificationShortText(String title, String msg) {
        if (title == null || title.isEmpty()) {
            title = "Fall Detection";
        }

        PendingIntent contentIntent;
        try {
            contentIntent = PendingIntent.getActivity(context, 0,
                    getIntentToStart(), PendingIntent.FLAG_CANCEL_CURRENT);

        } catch (Exception e) {
            contentIntent = null;
            e.printStackTrace();
        }

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ic_launcher);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setContentText(msg)
                .setAutoCancel(true)
                .setContentIntent(contentIntent);

        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(9876, mBuilder.build());

    }


}
